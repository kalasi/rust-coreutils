use std::env;

fn main() {
    if env::args().count() > 1 {
        print!("{}", env::args().nth(1).unwrap());
    }

    for arg in env::args().skip(2) {
        print!(" {}", arg);
    }

    println!("");
}
