use std::{env, process};

static NAME: &'static str = "yes";
static VERSION: &'static str = env!("CARGO_PKG_VERSION");

fn main() {
    process::exit(yes(env::args().collect()));
}

fn yes(args: Vec<String>) -> i32 {
    // Check if '--help' is present in the arglist, and if so, print the help.
    if args.contains(&String::from("--help")) {
        help();
    } else if args.contains(&String::from("--version")) {
        version();
    } else {
        let arg_list: Vec<String> = env::args().skip(1).collect();

        let text: String = if arg_list.len() == 0 {
            String::from("y")
        } else {
            arg_list.join(" ")
        };

        loop {
            println!("{}", text);
        }
    }

    // Under no circumstances should this fail, so the exit code should always
    // be 0, probably.
    0
}

fn help() {
    println!("Usage: yes [STRING]...");
    println!("  or:  yes OPTION");
    println!("Repeatedly output a line with all specified STRING(s), or \
                 'y'.");
    println!("");
    println!("      --help     display this help and exit");
    println!("      --version  output version information and exit");
}

fn version() {
    println!("{} (tcoreutils) {}", NAME, VERSION);
}
